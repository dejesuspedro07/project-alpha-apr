from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    list = Project.objects.filter(owner=request.user)
    context = {"list_projects": list}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    list = get_object_or_404(Project, id=id)
    context = {"show_project": list}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
        context = {
            "form": form,
        }
    return render(request, "projects/create.html", context)
