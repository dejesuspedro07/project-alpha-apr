from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import TaskForm


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("show_project", id=item.project.id)
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    category = Task.objects.filter(
        assignee=request.user,
    ).distinct()
    context = {
        "show_my_tasks": category,
    }
    return render(request, "tasks/mine.html", context)
